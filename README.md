# website-checkers

A compendium of tools/resources for checking URLs (and their contents), for validation/monitoring/cache warming/site migration, etc

## website-checker.sh

`website-checker.sh`: generates a CSV output of {URL},{HTTP_RESPONSE_CODE} given a list to be checked.

- Acquire an up to date cabundle.pem from https://curl.haxx.se/docs/caextract.html
- Create an urls.txt containing one complete URL per line. HTTP/HTTPS
- Run `./website-checker.sh`
- Retrieve results in `results.csv`

Note that this does not check for specific content being returned on any page/head result. URLs that fail to fetch (i.e. DNS failure) will return a code of 000.

Based on: https://gist.github.com/dominic-p/8729136

## wget-crawl.sh

Example of some useful wget options to crawl a site and output a log. This is configured to record output which can be used to list the URLs of a site to be tested by `website-checker.sh`

```shell
$ wget-crawl.sh https://example.com
```

This will create a wget.txt and wget-err.txt. The wget.txt can be parsed by `parse_wgetlog.sh`

## parse_wgetlog.sh

This will parse the list of the `wget-crawl.sh` to prepare it for `website-checker.sh`.

```shell
$ parse-wgetlog.sh wget.txt example.com > urls.txt
```

## Notes about other options

GUI:
[HTTrack](https://www.httrack.com/)

OK - not exactly a GUI, but there's a web ui for it also.

The following packages are available on Debian/Ubuntu:
httrack - command-like UI for downloading a web site.
webhttrack - web-based front-end for httrack

Install both via: `apt install httrack webhttrack`
Run the web ui via: `webhttrack lang 1`

Programmable:
BeautifulSoup - see [this great article](https://realpython.com/python-web-scraping-practical-introduction/) at realpython.com
