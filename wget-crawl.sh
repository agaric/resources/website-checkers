#!/usr/bin/env bash
set -eu
# Supply one argument -
# $1 - url
# e.g. wget-crawl.sh "https://example.com"

# WGET arguments:
# --delete-after, remove downloaded stuff afterwards.
# --nd, no directory structure
# -e robots=off, ignore robots.txt
# -prb, *p=get page requisites* r=recurse b=background
# -o, output log
# -nv, not verbose

# This version actually crawls, and dowloads all files... good cache warming.
# wget --delete-after -nd -e robots=off -prb -o wget-log -nv $1

# UNTESTED - processing log file notes:
# grep -B1 "ERROR" wget-log     - filter errors and preceeding line, which is the corresponding url.
# paste -s ",\n" wget-log       - join lines, separated with the cycle of listed delims
# (i.e. each two lines are made into one, separated by comma)

# Alternatively:
# spider doesn't actually do the downloads - it merely checks the links, so not good for cache warming,
# but rather validation/spidering/collecting URL list only.
# wget --spider -nd -e robots=off -prb -o wget-log -nv $1
#
#
# Other sources used:
# - https://coderwall.com/p/c0xsxw/crawling-all-uris-with-wget-and-grep
# - https://www.whatismybrowser.com/developers/tools/wget-wizard/
if [ $# -lt 1 ]; then
  echo "Must supply URL"
  exit 1
fi
url=$1
level=${2:-9}
wait=${3:-1}
outfile=${4:-wget.txt}
errfile=${5:-wget-err.txt}
code_rejects="py,php,cpp,rb"
image_rejects="jpg,jpeg,png,gif,js,css,svg,tiff,bmp,psd,ico"
font_rejects="fnt,fon,otf,ttf"
misc_rejects="log,tar,htaccess"
rejects=${5:-"${code_rejects},${image_rejects},${font_rejects},${misc_rejects}"}
wget --spider --no-verbose --recursive -e robots=off --level=${level} --no-directories \
  --reject="${rejects}" --wait=${wait} --random-wait ${url} \
  --append-output=${outfile} 2>> ${errfile}
