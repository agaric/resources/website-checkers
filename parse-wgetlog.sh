#!/usr/bin/env sh
set -eu
if [ $# -lt 2 ]; then
  echo "You must supply the file, and the site domain"
  exit 1
fi
file=$1
domain=$2
new_domain=${3:-$2}
leader=${4:-https}
cat ${file} | grep -oe "${domain}[^ ]*" | sort | uniq | sed -e "s/^${domain}/${leader}:\/\/${new_domain}/"
