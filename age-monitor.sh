#!/bin/bash
# Watches the age header, and prints out the last value when it resets.
# Useful to, for example, see how long CloudFlare caches a page before refreshing their copy.
# Usage:
# age-monitor.sh url {sleep seconds}
set -euo pipefail
age=0
last_age=99999 # ~27hrs
sleeps=${2:-10}
check=$(mktemp)
echo "Using tmp file ${check}"
while true; do
  wget -S --spider "${1}" &> ${check}
  age=$(grep -iF " age: " ${check} | cut -d: -f2 | sed "s/ //" || echo "-1")
  if [ $age -lt $last_age ]; then
    echo "Age reset/starting at: ${last_age} (current age ${age})"
    if [ $last_age -ne 99999 ]; then
      cat ${check}
      rm ${check}
      exit
    fi
  else
    echo "Current age: ${age}"
  fi
  last_age=$age
  sleep ${sleeps}
done
