#!/bin/bash
# Given a file (hard-coded to redirect-checks.csv)
# split the file by comma's. The first field is the URL to visit
# the second field is where the URL should redirect to.
# Will report failure if this is not correct.
# Also reports failure if this is not the ONLY redirect
while read line; do
	url=$(echo ${line}|cut -f1 -d,)
	redir="Location: $(echo ${line}|cut -f2 -d,) [following]"
	test=$(wget "${url}" 2>&1 | grep Location:)
	if [ "${test}" != "${redir}" ]; then
		echo -e "FAIL! Url: ${url}\ndoes not redirect to ${redir}"
		echo "Test result: ${test}"
	else
		echo "Passed (${url})"
	fi
done < redirect-checks.csv
