#!/usr/bin/env sh
set -eu
# TODO:
# Double-check curl args
# Potentially add validation search string back in.
# Move curl args to var(s)

url_list=${1:-urls.txt}
target=${2:-results.csv}
cabundle=cacert.pem

userauth=""
#userauth="-u user:password"

if ! [ -f "${cabundle}" ]; then
  echo "NO CA Bundle found. Acquiring latest from:"
  echo "https://curl.se/ca/cacert.pem"
  wget "https://curl.se/ca/cacert.pem" || (echo "Unable to acquire CA bundle"; exit 1)
fi

if [ -f "${target}" ]; then
  echo "${target} already exists - results will be appended"
fi

# Loop through all of the URLs and cURL them
while read siteurl
do
  # curl flags
  # --location = Follow HTTP redirects
  # --include = Include the HTTP headers in the output
  # --head = ONLY get the headers
  # --silent = Don't show progress bar
  # --max-time = How long to allow curl to run for each try (connection and download)
  # --cacert = See comment above
  # --user-agent = The user agent string to use
  # --write-out = Additional information for curl to write to its output

  result=$(curl ${userauth} --location --head --silent --max-time 12 --cacert ${cabundle} \
    --user-agent "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0" \
    --write-out "\nHTTP Code: %{http_code}\n" ${siteurl} 2>&1)
  result_code=$(echo "${result}"|tail -n1 |sed "s/HTTP Code: //")
  echo "\"${siteurl}\",\"${result_code}\"" >> ${target}
done < ${url_list}
echo "Done"
